var EMP_ARRAY = [];
document.getElementById("btnCapNhat").disabled = true;

// Get data from local storage when reload
let dataLocalStorage = localStorage.getItem("EMPLOYEE_LOCAL");
console.log("dataLocalStorage :", dataLocalStorage);
// Check data local storage có tồn tại hay không
if (dataLocalStorage != null) {
  // Nếu có data từ local storage thì parse sang mảng
  // Vì dataLocalStorage là JSON
  var dataArray = JSON.parse(dataLocalStorage);
  // Vì data từ local storage không có method
  // Duyệt mảng dataArray để convert thành mảng có method
  EMP_ARRAY = dataArray.map(function (item) {
    var emp = new Employee(
      item.account,
      item.name,
      item.email,
      item.password,
      item.date,
      item.basicSalary,
      item.position,
      item.hour
    );
    return emp;
  });
  render(EMP_ARRAY);
}
function addEmployee() {
  var emp = getDataFromForm();
  var isValid = true;

  isValid =
    validateDuplicate(emp.account, EMP_ARRAY) &&
    validateLengthInput(emp.account, "tbTKNV", 4, 6) &&
    validateAccount(emp.account) &&
    validateName(emp.name) &&
    validateEmail(emp.email) &&
    validatePassword(emp.password) &&
    validateDate(emp.date) &&
    validateSalary(emp.basicSalary) &&
    validatePosition(emp.position, "tbChucVu") &&
    validateHour(emp.hour);

  console.log("isValid :", isValid);
  if (isValid) {
    EMP_ARRAY.push(emp);
    var EMP_JSON = JSON.stringify(EMP_ARRAY);
    localStorage.setItem("EMPLOYEE_LOCAL", EMP_JSON);
    render(EMP_ARRAY);
  }
}
function deleteEmployee(account) {
  //splice(viTri, soluong = 1)
  var viTri = findID(account, EMP_ARRAY);

  if (viTri != -1) {
    EMP_ARRAY.splice(viTri, 1);
    var EMP_JSON = JSON.stringify(EMP_ARRAY);
    localStorage.setItem("EMPLOYEE_LOCAL", EMP_JSON);
    render(EMP_ARRAY);
  }
}
function changeInformationEmployee(account) {
  var viTri = findID(account, EMP_ARRAY);
  if (viTri != -1) {
    document.getElementById("myModal").style.display = "block";
    document.getElementById("myModal").classList.add("show");
    document.getElementById("myModal").style.overflow = "auto";

    var emp = EMP_ARRAY[viTri];
    document.getElementById("tknv").value = `${emp.account}`;
    document.getElementById("tknv").disabled = true;
    document.getElementById("btnThemNV").disabled = true;
    document.getElementById("btnCapNhat").disabled = false;
    document.getElementById("tbCapNhatThanhCong").innerHTML = ``;

    document.getElementById("name").value = `${emp.name}`;
    document.getElementById("email").value = `${emp.email}`;
    document.getElementById("password").value = `${emp.password}`;
    document.getElementById("datepicker").value = `${emp.date}`;
    document.getElementById("luongCB").value = `${emp.basicSalary}`;
    document.getElementById("chucvu").value = `${emp.position}`;
    document.getElementById("gioLam").value = `${emp.hour}`;
  }
}
function closeModal() {
  document.getElementById("myModal").classList.remove("show");
  document.getElementById("myModal").style.display = "none";
  document.getElementById("btnThemNV").disabled = false;
  document.getElementById("tknv").disabled = false;
  document.getElementById("btnCapNhat").disabled = true;

  document.getElementById("tbTen").style.display = "none";
  document.getElementById("tbTen").innerHTML = ``;
  document.getElementById("tbEmail").style.display = "none";
  document.getElementById("tbEmail").innerHTML = ``;
  document.getElementById("tbMatKhau").style.display = "none";
  document.getElementById("tbMatKhau").innerHTML = ``;
  document.getElementById("tbNgay").style.display = "none";
  document.getElementById("tbNgay").innerHTML = ``;
  document.getElementById("tbLuongCB").style.display = "none";
  document.getElementById("tbLuongCB").innerHTML = ``;
  document.getElementById("tbChucVu").style.display = "none";
  document.getElementById("tbChucVu").innerHTML = ``;
  document.getElementById("tbGiolam").style.display = "none";
  document.getElementById("tbGiolam").innerHTML = ``;
}

function updateEmployee() {
  var emp = getDataFromForm();
  var isValid = true;

  isValid =
    validateLengthInput(emp.account, "tbTKNV", 4, 6) &&
    validateAccount(emp.account) &&
    validateName(emp.name) &&
    validateEmail(emp.email) &&
    validatePassword(emp.password) &&
    validateDate(emp.date) &&
    validateSalary(emp.basicSalary) &&
    validatePosition(emp.position, "tbChucVu") &&
    validateHour(emp.hour);

  console.log("isValid :", isValid);

  if (isValid) {
    deleteEmployee(emp.account);
    EMP_ARRAY.push(emp);
    var EMP_JSON = JSON.stringify(EMP_ARRAY);
    localStorage.setItem("EMPLOYEE_LOCAL", EMP_JSON);
    render(EMP_ARRAY);
    document.getElementById("tbCapNhatThanhCong").style.display =
      "inline-block";
    document.getElementById(
      "tbCapNhatThanhCong"
    ).innerHTML = `Cập nhật thành công`;
  } else {
    document.getElementById("tbCapNhatThanhCong").style.display = "none";
  }
}
