function Employee(
  _account,
  _name,
  _email,
  _password,
  _date,
  _basicSalary,
  _position,
  _hour,
  _totalSalary,
  _typeOfEmployee
) {
  this.account = _account;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.date = _date;
  this.basicSalary = _basicSalary;
  this.position = _position;
  this.hour = _hour;
  this.totalSalary = function () {
    switch (this.position) {
      case "Sếp":
        return this.basicSalary * 3;
      case "Trưởng phòng":
        return this.basicSalary * 2;
      case "Nhân viên":
        return this.basicSalary;
    }
  };
  this.typeOfEmployee = function () {
    var text = "";
    if (this.hour >= 192) {
      text += "Xuất sắc";
    } else if (this.hour >= 176) {
      text += "Giỏi";
    } else if (this.hour >= 160) {
      text += "Khá";
    } else {
      text += "Trung bình";
    }
    return text;
  };
}
