function getDataFromForm() {
  var _account = document.getElementById("tknv").value;
  var _name = document.getElementById("name").value;
  var _email = document.getElementById("email").value;
  var _password = document.getElementById("password").value;
  var _date = document.getElementById("datepicker").value;
  var _basicSalary = document.getElementById("luongCB").value * 1;
  var _position = document.getElementById("chucvu").value;
  var _hour = document.getElementById("gioLam").value * 1;
  return new Employee(
    _account,
    _name,
    _email,
    _password,
    _date,
    _basicSalary,
    _position,
    _hour
  );
}
function render(array) {
  var contentHTML = "";
  for (let i = 0; i < array.length; i++) {
    var emp = array[i];
    var contentTr = `<tr>
                            <td>${emp.account}</td>
                            <td>${emp.name}</td>
                            <td>${emp.email}</td>
                            <td>${emp.date}</td>
                            <td>${emp.position}</td>
                            <td>${emp.totalSalary()}</td>
                            <td>${emp.typeOfEmployee()}</td>

                            <td onclick="deleteEmployee('${
                              emp.account
                            }')" class="btn btn-danger">Xoá</td>
                            <td onclick="changeInformationEmployee('${
                              emp.account
                            }')" class="btn btn-warning">Sửa</td>
                            
                        </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = `${contentHTML}`;
}
function findID(id, array) {
  var viTri = -1;
  for (var i = 0; i < array.length; i++) {
    if (array[i].account == id) {
      viTri = i;
    }
  }
  return viTri;
}
