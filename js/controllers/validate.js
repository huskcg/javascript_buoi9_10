function validateDuplicate(account, array) {
  var viTri = array.findIndex(function (item) {
    return item.account == account;
  });
  if (viTri != -1) {
    alert("Trùng tài khoản");
    return false;
  } else {
    return true;
  }
}
function validateLengthInput(value, idError, min, max) {
  var length = value.length;
  if (length < min || length > max) {
    document.getElementById(idError).style.display = "inline-block";
    document.getElementById(
      idError
    ).innerHTML = `Độ dài phải từ ${min} đến ${max} kí tự`;
    return false;
  } else {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerHTML = ``;
    return true;
  }
}
function validateAccount(value) {
  const regexAccount = /^\d+$/;
  var isAccount = regexAccount.test(value);
  if (isAccount) {
    document.getElementById("tbTKNV").style.display = "none";
    document.getElementById("tbTKNV").innerHTML = ``;
    return true;
  } else {
    document.getElementById("tbTKNV").style.display = "inline-block";
    document.getElementById("tbTKNV").innerHTML = `Tài khoản phải là số`;
    return false;
  }
}
function validateName(value) {
  const regexName =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
  var isName = regexName.test(value);
  if (isName) {
    document.getElementById("tbTen").style.display = "none";
    document.getElementById("tbTen").innerHTML = ``;
    return true;
  } else {
    document.getElementById("tbTen").style.display = "inline-block";
    document.getElementById(
      "tbTen"
    ).innerHTML = `Tên nhân viên phải là chữ, không để trống`;
    return false;
  }
}
function validateEmail(value) {
  const regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = regexEmail.test(value);
  if (isEmail) {
    document.getElementById("tbEmail").style.display = "none";
    document.getElementById("tbEmail").innerHTML = ``;
    return true;
  } else {
    document.getElementById("tbEmail").style.display = "inline-block";
    document.getElementById("tbEmail").innerHTML = `Email không đúng định dạng`;
    return false;
  }
}
function validatePassword(value) {
  const regexPassword =
    /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,10}$/;
  var isPassword = regexPassword.test(value);
  if (isPassword) {
    document.getElementById("tbMatKhau").style.display = "none";
    document.getElementById("tbMatKhau").innerHTML = ``;
    return true;
  } else {
    document.getElementById("tbMatKhau").style.display = "inline-block";
    document.getElementById(
      "tbMatKhau"
    ).innerHTML = `Mật khẩu từ 6-10 ký tự, chứa ít nhất 1 ký tự viết hoa, 1 ký tự số, 1 ký tự đặc biệt`;
    return false;
  }
}
function validateDate(value) {
  const regexDate =
    /(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$/;
  var isDate = regexDate.test(value);
  if (isDate) {
    document.getElementById("tbNgay").style.display = "none";
    document.getElementById("tbNgay").innerHTML = ``;
    return true;
  } else {
    document.getElementById("tbNgay").style.display = "inline-block";
    document.getElementById(
      "tbNgay"
    ).innerHTML = `Sai định dạng MM/DD/YYYY hoặc ngày không tồn tại`;
    return false;
  }
}
function validateSalary(value) {
  const regexSalary = /^\d+$/;
  var isSalary = regexSalary.test(value);
  if (isSalary) {
    document.getElementById("tbLuongCB").style.display = "none";
    document.getElementById("tbLuongCB").innerHTML = ``;
    if (value >= 1e6 && value <= 20e6) {
      return true;
    } else {
      document.getElementById("tbLuongCB").style.display = "inline-block";
      document.getElementById(
        "tbLuongCB"
      ).innerHTML = `Lương cơ bản từ 1.000.000 -> 20.000.000`;
      return false;
    }
  } else {
    document.getElementById("tbLuongCB").style.display = "inline-block";
    document.getElementById("tbLuongCB").innerHTML = `Tổng lương phải là số`;
  }
}
function validatePosition(value, idError) {
  if (value == "Chọn chức vụ") {
    document.getElementById(idError).style.display = "inline-block";
    document.getElementById(idError).innerHTML = `Chức vụ không hợp lệ`;
    return false;
  } else {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerHTML = ``;
    return true;
  }
}
function validateHour(value) {
  const regexHour = /^\d+$/;
  var isHour = regexHour.test(value);
  if (isHour) {
    document.getElementById("tbGiolam").style.display = "none";
    document.getElementById("tbGiolam").innerHTML = ``;
    if (value >= 80 && value <= 200) {
      return true;
    } else {
      document.getElementById("tbGiolam").style.display = "inline-block";
      document.getElementById(
        "tbGiolam"
      ).innerHTML = `Giờ làm trong khoản 80 -> 200 giờ`;
      return false;
    }
  } else {
    document.getElementById("tbGiolam").style.display = "inline-block";
    document.getElementById("tbGiolam").innerHTML = `Giờ làm phải là số`;
  }
}
